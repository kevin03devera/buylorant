import 'package:flutter/material.dart';
import 'package:valoecommerce/Login.dart';
import 'package:valoecommerce/Region.dart';
import 'package:valoecommerce/Bundles.dart';
import 'package:valoecommerce/Cart.dart';


void main() {
  runApp(const Main());
}

class Main extends StatelessWidget {
  const Main({super.key});

  @override
  Widget build(BuildContext context) {
    return  const MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Region(),
    );
  }
}
