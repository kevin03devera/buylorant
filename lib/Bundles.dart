import 'dart:ffi';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:valoecommerce/Cart.dart';
import 'package:valoecommerce/Profile.dart';
import 'package:valoecommerce/Region.dart';
import 'package:valoecommerce/Bundles.dart';
import 'package:valoecommerce/Shop.dart';
import 'package:valoecommerce/navbar.dart';

class Bundles extends StatelessWidget {
  const Bundles({super.key});

  void _Cart(BuildContext context) {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => const Cart()),
    );
  }

  void _Region(BuildContext context) {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => const Region()),
    );
  }

  void _Profile(BuildContext context) {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => const Profile()),
    );
  }

  void _Shop(BuildContext context) {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => const Shop()),
    );
  }

  void _Bundles(BuildContext context) {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => const Bundles()),
    );
  }

  void _navbar(BuildContext context) {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => const navbar()),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: AppBar(
        backgroundColor: Colors.red,
        title: const Text(
          'Bundles',
          style: TextStyle(
            fontSize: 25,
            fontWeight: FontWeight.bold,
            color: Colors.white,
          ),
        ),
        centerTitle: true,
        actions: <Widget>[
          IconButton(
            onPressed: () {
              _Cart(context);
            },
            icon: const Icon(Icons.shopping_cart_outlined),
            iconSize: 30,
          ),
        ],
      ),
      drawer: Drawer(
        backgroundColor: Colors.black,
        child: ListView(
          children: [
            DrawerHeader(
              decoration: const BoxDecoration(
                color: Color.fromARGB(255, 0, 0, 0),
              ),
              child: Image.asset(
                'assets/images/jett.png.webp',
                fit: BoxFit.contain,
              ),
            ),
            ListTile(
              leading: const Icon(
                Icons.wallet,
                color: Colors.white,
                size: 30,
              ),
              title: const Text(
                'Bundles',
                style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontSize: 20),
              ),
              onTap: () {
                _Bundles(context);
              },
            ),
            ListTile(
              leading: const Icon(
                Icons.shopping_basket_outlined,
                color: Colors.white,
                size: 30,
              ),
              title: const Text(
                'Shop',
                style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontSize: 20),
              ),
              onTap: () {
                _Shop(context);
              },
            ),
            ListTile(
              leading: const Icon(
                Icons.shopping_cart_outlined,
                color: Colors.white,
                size: 30,
              ),
              title: const Text(
                'Cart',
                style: TextStyle(
                    color: Color.fromARGB(
                      255,
                      255,
                      255,
                      255,
                    ),
                    fontWeight: FontWeight.bold,
                    fontSize: 20),
              ),
              onTap: () {
                _Cart(context);
              },
            ),
            ListTile(
              leading: const Icon(
                Icons.person_outline_rounded,
                color: Colors.white,
                size: 30,
              ),
              title: const Text(
                'Profile',
                style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontSize: 20),
              ),
              onTap: () {
                _Profile(context);
              },
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                const Padding(
                  padding: EdgeInsets.only(top: 250),
                ),
                GestureDetector(
                  onTap: () {
                    _Region(context);
                  },
                  child: const Text(
                    'Log out',
                    style: TextStyle(
                      color: Colors.red,
                      fontSize: 30,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                const Padding(
                  padding: EdgeInsets.only(top: 400, right: 50),
                ),
              ],
            ),
          ],
        ),
      ),
      body: SingleChildScrollView(
        child: Center(
          child: Column(
            children: [
              Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(top: 0),
                    child: Container(
                      padding: const EdgeInsets.all(0),
                      decoration: const BoxDecoration(
                          color: Color.fromARGB(255, 0, 0, 0)),
                      child: Column(
                        children: [
                          Stack(
                            alignment: Alignment.center,
                            children: [
                              BackdropFilter(
                                filter: ImageFilter.blur(sigmaX: 2, sigmaY: 2),
                                child: Image.asset('assets/images/reaver.jpg'),
                              ),
                              Positioned(
                                top: 0,
                                child: Container(
                                  padding: const EdgeInsets.all(80),
                                  child: const Text(
                                    'Reaver 2.0 \n   Bundle',
                                    style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 40,
                                      color: Colors.white,
                                      shadows: [
                                        Shadow(
                                          color: Colors.black,
                                          blurRadius: 2,
                                          offset: Offset(2, 2),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
              Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(top: 0),
                    child: Container(
                      padding: const EdgeInsets.only(left: 5, right: 5),
                      decoration: const BoxDecoration(
                          color: Color.fromARGB(255, 0, 0, 0)),
                      child: Column(
                        children: [
                          Stack(
                            alignment: Alignment.center,
                            children: [
                              Image.asset('assets/images/phantom.jpg'),
                              Positioned(
                                top: 0,
                                child: Container(
                                  padding: const EdgeInsets.only(
                                      top: 160, right: 240),
                                  child: const Text(
                                    'Reaver Phantom\n120 Php',
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 14,
                                        color: Colors.white,
                                        shadows: [
                                          Shadow(
                                            color: Colors.black,
                                            blurRadius: 2,
                                            offset: Offset(2, 2),
                                          ),
                                        ]),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
              Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(top: 0),
                    child: Container(
                      padding: const EdgeInsets.only(left: 5, right: 5),
                      decoration: const BoxDecoration(
                          color: Color.fromARGB(255, 0, 0, 0)),
                      child: Column(
                        children: [
                          Stack(
                            alignment: Alignment.center,
                            children: [
                              Image.asset('assets/images/karambit.jpg'),
                              Positioned(
                                top: 0,
                                child: Container(
                                  padding: const EdgeInsets.only(
                                      top: 160, right: 240),
                                  child: const Text(
                                    'Reaver Karambit\n150 Php',
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 14,
                                        color: Colors.white),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
              Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(top: 0),
                    child: Container(
                      padding: const EdgeInsets.only(left: 5, right: 5),
                      decoration: const BoxDecoration(
                          color: Color.fromARGB(255, 0, 0, 0)),
                      child: Column(
                        children: [
                          Stack(
                            alignment: Alignment.center,
                            children: [
                              Image.asset('assets/images/ghost.jpg'),
                              Positioned(
                                top: 0,
                                child: Container(
                                  padding: const EdgeInsets.only(
                                      top: 160, right: 250),
                                  child: const Text(
                                    'Reaver Ghost\n120 Php',
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 14,
                                        color: Colors.white),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
              Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(top: 0),
                    child: Container(
                      padding: const EdgeInsets.only(left: 5, right: 5),
                      decoration: const BoxDecoration(
                          color: Color.fromARGB(255, 0, 0, 0)),
                      child: Column(
                        children: [
                          Stack(
                            alignment: Alignment.center,
                            children: [
                              Image.asset('assets/images/spectre.jpg'),
                              Positioned(
                                top: 0,
                                child: Container(
                                  padding: const EdgeInsets.only(
                                      top: 160, right: 240),
                                  child: const Text(
                                    'Reaver Spectre\n120 Php',
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 14,
                                        color: Colors.white),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
