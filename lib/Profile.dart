import 'package:flutter/material.dart';
import 'package:valoecommerce/Region.dart';
import 'package:valoecommerce/Bundles.dart';
import 'package:valoecommerce/Cart.dart';
import 'package:valoecommerce/Shop.dart';
import 'package:valoecommerce/main.dart';

class Profile extends StatelessWidget {
  const Profile({super.key});

  void _Cart(BuildContext context) {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => const Cart()),
    );
  }

  void _Profile(BuildContext context) {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => const Profile()),
    );
  }

  void _Bundles(BuildContext context) {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => const Bundles()),
    );
  }

  void _Shop(BuildContext context) {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => const Shop()),
    );
  }

  void _Region(BuildContext context) {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => const Region()),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: AppBar(
        title: const Text(
          'Profile',
          style: TextStyle(
              fontSize: 25, fontWeight: FontWeight.bold, color: Colors.white),
        ),
        centerTitle: true,
        backgroundColor: Colors.red,
      ),
      drawer: Drawer(
        backgroundColor: Colors.black,
        child: ListView(
          children: [
            DrawerHeader(
              decoration: const BoxDecoration(
                color: Color.fromARGB(255, 0, 0, 0),
              ),
              child: Image.asset(
                'assets/images/jett.png.webp',
                fit: BoxFit.contain,
              ),
            ),
            ListTile(
              leading: const Icon(
                Icons.wallet,
                color: Colors.white,
                size: 30,
              ),
              title: const Text(
                'Bundles',
                style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontSize: 20),
              ),
              onTap: () {
                _Bundles(context);
              },
            ),
            ListTile(
              leading: const Icon(
                Icons.shopping_basket_outlined,
                color: Colors.white,
                size: 30,
              ),
              title: const Text(
                'Shop',
                style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontSize: 20),
              ),
              onTap: () {
                _Shop(context);
              },
            ),
            ListTile(
              leading: const Icon(
                Icons.shopping_cart_outlined,
                color: Colors.white,
                size: 30,
              ),
              title: const Text(
                'Cart',
                style: TextStyle(
                    color: Color.fromARGB(
                      255,
                      255,
                      255,
                      255,
                    ),
                    fontWeight: FontWeight.bold,
                    fontSize: 20),
              ),
              onTap: () {
                _Cart(context);
              },
            ),
            ListTile(
              leading: const Icon(
                Icons.person_outline_rounded,
                color: Colors.white,
                size: 30,
              ),
              title: const Text(
                'Profile',
                style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontSize: 20),
              ),
              onTap: () {
                _Profile(context);
              },
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                const Padding(
                  padding: EdgeInsets.only(top: 250),
                ),
                GestureDetector(
                  onTap: () {
                    _Region(context);
                  },
                  child: const Text(
                    'Log out',
                    style: TextStyle(
                      color: Colors.red,
                      fontSize: 30,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                const Padding(
                  padding: EdgeInsets.only(top: 400, right: 50),
                ),
              ],
            ),
          ],
        ),
      ),
      body: SafeArea(
        child: Center(
          child: Column(
            children: [
              Row(
                children: const [
                  Padding(
                    padding: EdgeInsets.only(top: 10, left: 15),
                    child: Text(
                      'My Purchase',
                      style: TextStyle(color: Colors.white, fontSize: 30),
                    ),
                  ),
                ],
              ),
              Row(
                children: const [
                  Padding(
                    padding: EdgeInsets.only(top: 30, left: 5),
                    child: Text(
                      'To Ship',
                      style: TextStyle(
                        color: Colors.red,
                        fontSize: 20,
                        shadows: [
                          Shadow(
                            color: Colors.grey,
                            offset: Offset(1, 1),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
              Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(top: 0),
                    child: Container(
                      padding: const EdgeInsets.only(left: 5, right: 5),
                      decoration: const BoxDecoration(
                          color: Color.fromARGB(255, 0, 0, 0)),
                      child: Column(
                        children: [
                          Stack(
                            alignment: Alignment.center,
                            children: [
                              Image.asset('assets/images/phantom.jpg'),
                              Positioned(
                                top: 0,
                                child: Container(
                                  padding: const EdgeInsets.only(
                                      top: 150, right: 240),
                                  child: const Text(
                                    'Reaver Phantom\n120 Php',
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 14,
                                        color: Colors.white,
                                        shadows: [
                                          Shadow(
                                            color: Colors.black,
                                            blurRadius: 2,
                                            offset: Offset(2, 2),
                                          ),
                                        ]),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
