import 'package:flutter/material.dart';
import 'package:valoecommerce/Bundles.dart';
import 'package:valoecommerce/Login.dart';
import 'package:valoecommerce/main.dart';

class Region extends StatelessWidget {
  const Region({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      body: SafeArea(
        child: Center(
          child: Center(
            child: Column(
              children: [
                Row(
                  children: const [
                    Padding(
                      padding: EdgeInsets.only(top: 30, left: 25),
                      child: Text(
                        'Select Region',
                        style: TextStyle(color: Colors.white, fontSize: 30),
                      ),
                    ),
                  ],
                ),
                Row(
                  children: const [
                    Padding(
                      padding: EdgeInsets.only(top: 20, left: 25),
                      child: Text(
                        'Please make sure you select the correct region,\notherwise your shop will be inaccurate.',
                        style: TextStyle(
                            color: Color.fromARGB(255, 214, 116, 4),
                            fontSize: 15),
                      ),
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    const Padding(
                      padding: EdgeInsets.only(top: 10, left: 25),
                      child: Text(
                        'Europe (EU)',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                    Checkbox(
                      activeColor: Color.fromARGB(255, 255, 255, 255),
                      checkColor: Color.fromARGB(255, 0, 0, 0),
                      value: true,
                      onChanged: (bool? value) {},
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    const Padding(
                      padding: EdgeInsets.only(top: 5, left: 25),
                      child: Text(
                        'Asia-Pacific (APAC)',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                    Checkbox(
                      activeColor: Colors.red,
                      checkColor: Colors.white,
                      value: true,
                      onChanged: (bool? value) {},
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    const Padding(
                      padding: EdgeInsets.only(top: 5, left: 25),
                      child: Text(
                        'North America (NA)',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                    Checkbox(
                      activeColor: Color.fromARGB(255, 255, 255, 255),
                      checkColor: Color.fromARGB(255, 0, 0, 0),
                      value: true,
                      onChanged: (bool? value) {},
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    const Padding(
                      padding: EdgeInsets.only(top: 5, left: 25),
                      child: Text(
                        'Korea (KR)',
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                    Checkbox(
                      activeColor: Color.fromARGB(255, 255, 255, 255),
                      checkColor: Color.fromARGB(255, 0, 0, 0),
                      value: true,
                      onChanged: (bool? value) {},
                    ),
                  ],
                ),
                const SizedBox(
                  height: 330,
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 30),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Row(
                        children: [
                          TextButton(
                            onPressed: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => const Login(),
                                ),
                              );
                            },
                            child: const Text(
                              'NEXT',
                              style: TextStyle(
                                  color: Color.fromARGB(255, 214, 116, 4),
                                  fontSize: 25,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
