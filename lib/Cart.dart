import 'package:flutter/material.dart';
import 'package:valoecommerce/Profile.dart';
import 'package:valoecommerce/Region.dart';
import 'package:valoecommerce/Bundles.dart';
import 'package:valoecommerce/Cart.dart';
import 'package:valoecommerce/Shop.dart';
import 'package:valoecommerce/main.dart';

class Cart extends StatelessWidget {
  const Cart({super.key});

  void _Cart(BuildContext context) {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => const Cart()),
    );
  }

  void _Bundles(BuildContext context) {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => const Bundles()),
    );
  }

  void _Profile(BuildContext context) {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => const Profile()),
    );
  }

  void _Shop(BuildContext context) {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => const Shop()),
    );
  }

  void _Region(BuildContext context) {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => const Region()),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: AppBar(
        title: const Text(
          'Cart',
          style: TextStyle(
              fontSize: 25, fontWeight: FontWeight.bold, color: Colors.white),
        ),
        centerTitle: true,
        backgroundColor: Colors.red,
      ),
      drawer: Drawer(
        backgroundColor: Colors.black,
        child: ListView(
          children: [
            DrawerHeader(
              decoration: const BoxDecoration(
                color: Color.fromARGB(255, 0, 0, 0),
              ),
              child: Image.asset(
                'assets/images/jett.png.webp',
                fit: BoxFit.contain,
              ),
            ),
            ListTile(
              leading: const Icon(
                Icons.wallet,
                color: Colors.white,
                size: 30,
              ),
              title: const Text(
                'Bundles',
                style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontSize: 20),
              ),
              onTap: () {
                _Bundles(context);
              },
            ),
            ListTile(
              leading: const Icon(
                Icons.shopping_basket_outlined,
                color: Colors.white,
                size: 30,
              ),
              title: const Text(
                'Shop',
                style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontSize: 20),
              ),
              onTap: () {
                _Shop(context);
              },
            ),
            ListTile(
              leading: const Icon(
                Icons.shopping_cart_outlined,
                color: Colors.white,
                size: 30,
              ),
              title: const Text(
                'Cart',
                style: TextStyle(
                    color: Color.fromARGB(
                      255,
                      255,
                      255,
                      255,
                    ),
                    fontWeight: FontWeight.bold,
                    fontSize: 20),
              ),
              onTap: () {
                _Cart(context);
              },
            ),
            ListTile(
              leading: const Icon(
                Icons.person_outline_rounded,
                color: Colors.white,
                size: 30,
              ),
              title: const Text(
                'Profile',
                style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontSize: 20),
              ),
              onTap: () {
                _Profile(context);
              },
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                const Padding(
                  padding: EdgeInsets.only(top: 250),
                ),
                GestureDetector(
                  onTap: () {
                    _Region(context);
                  },
                  child: const Text(
                    'Log out',
                    style: TextStyle(
                      color: Colors.red,
                      fontSize: 30,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                const Padding(
                  padding: EdgeInsets.only(top: 400, right: 50),
                ),
              ],
            ),
          ],
        ),
      ),
      body: SafeArea(
        child: Center(
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.only(top: 30),
                child: Container(
                  padding: const EdgeInsets.only(left: 10),
                  decoration: const BoxDecoration(
                    color: Color.fromARGB(255, 255, 255, 255),
                  ),
                  child: Column(
                    children: [
                      Stack(
                        alignment: Alignment.center,
                        children: [
                          Row(
                            children: [
                              Checkbox(
                                activeColor: Colors.red,
                                value: true,
                                onChanged: (bool? value) {},
                              ),
                              Image.asset(
                                'assets/images/phantom.jpg',
                                width: 200,
                                height: 100,
                                fit: BoxFit.cover,
                              ),
                              const SizedBox(
                                  width:
                                      30), // Add spacing between image and text
                              const Text(
                                'Php 120',
                                style: TextStyle(
                                    fontSize: 15, fontStyle: FontStyle.normal),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 30),
                child: Container(
                  padding: const EdgeInsets.only(left: 10),
                  decoration: const BoxDecoration(
                    color: Color.fromARGB(255, 255, 255, 255),
                  ),
                  child: Column(
                    children: [
                      Stack(
                        alignment: Alignment.center,
                        children: [
                          Row(
                            children: [
                              Checkbox(
                                value:
                                    false, // Replace false with your checkbox value
                                onChanged: (bool? value) {
                                  // Handle checkbox onChanged event
                                },
                              ),
                              Image.asset(
                                'assets/images/vandal.jpg',
                                width: 200,
                                height: 100,
                                fit: BoxFit.cover,
                              ),
                              const SizedBox(
                                  width:
                                      30), // Add spacing between image and text
                              const Text(
                                'Php 120',
                                style: TextStyle(
                                    fontSize: 15, fontStyle: FontStyle.normal),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 30),
                child: Container(
                  padding: const EdgeInsets.only(left: 10),
                  decoration: const BoxDecoration(
                    color: Color.fromARGB(255, 255, 255, 255),
                  ),
                  child: Column(
                    children: [
                      Stack(
                        alignment: Alignment.center,
                        children: [
                          Row(
                            children: [
                              Checkbox(
                                value:
                                    false, // Replace false with your checkbox value
                                onChanged: (bool? value) {
                                  // Handle checkbox onChanged event
                                },
                              ),
                              Image.asset(
                                'assets/images/minimaphantom.jpg',
                                width: 200,
                                height: 100,
                                fit: BoxFit.cover,
                              ),
                              const SizedBox(
                                  width:
                                      30), // Add spacing between image and text
                              const Text(
                                'Php 120',
                                style: TextStyle(
                                    fontSize: 15, fontStyle: FontStyle.normal),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 160),
                child: Row(
                  children: [
                    Container(
                      padding: const EdgeInsets.only(
                        top: 25,
                        bottom: 25,
                        left: 10,
                        right: 60,
                      ),
                      decoration: const BoxDecoration(
                        color: Color.fromARGB(255, 255, 255, 255),
                      ),
                      child: const Text(
                        'Total Payment:          PHP 120',
                        style: TextStyle(fontSize: 15),
                      ),
                    ),
                    GestureDetector(
                      onTap: () {
                        _Profile(context);
                      },
                      child: Container(
                        width: 100,
                        height: 68,
                        color: Colors.orange,
                        child: const Center(
                          child: Text(
                            'Place Order',
                            style: TextStyle(
                              fontSize: 15,
                              color: Colors.white,
                              fontStyle: FontStyle.normal,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
